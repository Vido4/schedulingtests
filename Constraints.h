#pragma once
#include <gtest/gtest.h>
#include "../nurse-scheduler-api/SoftPartialConstraints.h"
#include "../nurse-scheduler-api/HardPartialConstraints.h"

class ConstraintsTest : public ::testing::Test{
protected:
	static void setPattern(ShiftCombination &combination, std::string pattern) {
		combination.resize(pattern.size());
		for (int i = 0; i < pattern.size(); i++) {
			switch (pattern[i]) {
			case 'D':
				combination.changeShiftOnDay(i, ShiftCombination::ShiftType::DAY);
				break;
			case 'N':
				combination.changeShiftOnDay(i, ShiftCombination::ShiftType::NIGHT);
				break;
			case 'R':
				combination.changeShiftOnDay(i, ShiftCombination::ShiftType::REST);
				break;

			default:
				throw IncorrectPatternException();
			}
		}
	}

	static void setExtendedPattern(ShiftCombination &combination, std::string pattern) {
		combination.resizeExtended(pattern.size());
		for (int i = 0; i < pattern.size(); i++) {
			switch (pattern[i]) {
			case 'E':
				combination.changeExtendedShiftOnDay(i, ShiftCombination::ExtendedShiftType::EARLY);
				break;
			case 'D':
				combination.changeExtendedShiftOnDay(i, ShiftCombination::ExtendedShiftType::MID);
				break;
			case 'L':
				combination.changeExtendedShiftOnDay(i, ShiftCombination::ExtendedShiftType::LATE);
				break;
			case 'N':
				combination.changeExtendedShiftOnDay(i, ShiftCombination::ExtendedShiftType::NIGHTS);
				break;
			case 'R':
				combination.changeExtendedShiftOnDay(i, ShiftCombination::ExtendedShiftType::RESTING);
				break;

			default:
				throw IncorrectPatternException();
			}
		}
	}
};

class SoftConstraintsTest : public ConstraintsTest{
protected:
	SoftPartialConstraints softConstraints;
};

class HardConstraintsTest : public ConstraintsTest {
protected:
	HardPartialConstraints hardConstraints;
};