#include "Constraints.h"

TEST_F(HardConstraintsTest, hasProperRestAfterConsecutiveNights)
{
	ShiftCombination pattern;
	std::shared_ptr<Nurse> fullTimeNurse = std::make_shared<Nurse>(0, 36, false);
	setPattern(pattern, "NNRDDRR");
	EXPECT_FALSE(hardConstraints.hasProperRestAfterConsecutiveNights(pattern));
	setPattern(pattern, "DNNRRDD");
	EXPECT_TRUE(hardConstraints.hasProperRestAfterConsecutiveNights(pattern));
	setPattern(pattern, "NNNRRRD");
	EXPECT_TRUE(hardConstraints.hasProperRestAfterConsecutiveNights(pattern));
	setPattern(pattern, "DDDRNNN");
	EXPECT_TRUE(hardConstraints.hasProperRestAfterConsecutiveNights(pattern));
	setPattern(pattern, "DDDRNNR");
	EXPECT_TRUE(hardConstraints.hasProperRestAfterConsecutiveNights(pattern));
}

TEST_F(HardConstraintsTest, hasProperRestAfterSingleNight)
{
	ShiftCombination pattern;
	std::shared_ptr<Nurse> fullTimeNurse = std::make_shared<Nurse>(0, 36, false);
	setPattern(pattern, "NNRDDRR");
	EXPECT_TRUE(hardConstraints.hasProperRestAfterSingleNight(pattern));
	setPattern(pattern, "DDDRNDD");
	EXPECT_FALSE(hardConstraints.hasProperRestAfterSingleNight(pattern));
	setPattern(pattern, "DNRDDDD");
	EXPECT_TRUE(hardConstraints.hasProperRestAfterSingleNight(pattern));
}

TEST_F(HardConstraintsTest, hasMinimumRestAmountPerWeek)
{
	ShiftCombination pattern;
	std::shared_ptr<Nurse> fullTimeNurse = std::make_shared<Nurse>(0, 36, false);
	setPattern(pattern, "DDDDNDD");
	EXPECT_FALSE(hardConstraints.hasMinimumRestAmountPerWeek(pattern));
	setPattern(pattern, "RDDDNDD");
	EXPECT_TRUE(hardConstraints.hasMinimumRestAmountPerWeek(pattern));
}

TEST_F(HardConstraintsTest, hasMaximum3NightsPerWholeSchedule)
{
	ShiftCombination pattern;
	ShiftCombination midiSchedule(21);
	std::shared_ptr<Nurse> fullTimeNurse = std::make_shared<Nurse>(0, 36, false);
	setPattern(pattern, "DDRDNNN");
	EXPECT_TRUE(hardConstraints.hasMaximum3NightsPerWholeSchedule(pattern));
	setPattern(pattern, "DDRDDDD");
	EXPECT_TRUE(hardConstraints.hasMaximum3NightsPerWholeSchedule(pattern));
	setPattern(midiSchedule, "DDRDDDDDDRDDDDDDRDDDD");
	EXPECT_TRUE(hardConstraints.hasMaximum3NightsPerWholeSchedule(midiSchedule));
	setPattern(midiSchedule, "DDRDDDNNNRDDDDDDRDDDD");
	EXPECT_TRUE(hardConstraints.hasMaximum3NightsPerWholeSchedule(midiSchedule));
	setPattern(midiSchedule, "DDRDDDNNRDDDDDDRNNDDD");
	EXPECT_FALSE(hardConstraints.hasMaximum3NightsPerWholeSchedule(midiSchedule));
}

TEST_F(HardConstraintsTest, hasMaximum4HoursOvertimePerSchedule)
{
	ShiftCombination pattern;
	ShiftCombination midiSchedule(21);
	std::shared_ptr<Nurse> fullTimeNurse = std::make_shared<Nurse>(0, 32, false);
	setPattern(pattern, "DDRRDNN");
	EXPECT_TRUE(hardConstraints.hasMaximum4HoursOvertime(pattern, fullTimeNurse));
	setPattern(midiSchedule, "DDDDDDDRRDDDNNDRDDDDD");
	EXPECT_FALSE(hardConstraints.hasMaximum4HoursOvertime(midiSchedule, fullTimeNurse));
}

TEST_F(HardConstraintsTest, hasAtLeast2FreeWeekendsPerSchedule)
{
	ShiftCombination midiSchedule(21);
	std::shared_ptr<Nurse> fullTimeNurse = std::make_shared<Nurse>(0, 36, false);
	setPattern(midiSchedule, "DDNNDRRDDNNNRRDDDDDND");
	EXPECT_FALSE(hardConstraints.hasAtLeastNFreeWeekendsPerSchedule(midiSchedule, 2));
	setPattern(midiSchedule, "DDNNDRRDDNNNRRDDDDDRR");
	EXPECT_TRUE(hardConstraints.hasAtLeastNFreeWeekendsPerSchedule(midiSchedule, 2));
	setPattern(midiSchedule, "DDRRRDDRRDDDDRRDDDDDD");
	EXPECT_FALSE(hardConstraints.hasAtLeastNFreeWeekendsPerSchedule(midiSchedule, 2));
}