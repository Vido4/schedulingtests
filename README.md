# README #

### How do I get set up? ###

Clone this repository and launch it with Visual Studio. While compiling it missing packages from NuGet should be downloaded automatically, if it says you are missing them, enable automatic download of missing packages in NuGet. Tested on Visual Studio 2015. Please clone it in same directory as nurse-scheduler-api repository, as tests use libraries generated from api. In other case look below.

### Nurse-api library ###

Test program is looking for nurse-api library at directory "../nurse-scheduler-api" relative to its repository folder. If you wish to install it in different path, please change Additional Library directories settings (In VS15 Project properties -> Linker -> General -> Additional Library Directories)