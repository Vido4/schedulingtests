#include "Constraints.h"

TEST_F(SoftConstraintsTest, areIsolatedShiftsCorrectlyChecked)
{
	ShiftCombination pattern;
	setPattern(pattern, "DDDNNRN");
	EXPECT_EQ(softConstraints.getCostOfIsolatedShifts(pattern), 0);
	setPattern(pattern, "DRNRNDD");
	EXPECT_EQ(softConstraints.getCostOfIsolatedShifts(pattern), 1000);
	setPattern(pattern, "RRDRNRD");
	EXPECT_EQ(softConstraints.getCostOfIsolatedShifts(pattern), 2000);
}

TEST_F(SoftConstraintsTest, areNightSeriesCorrectlyChecked)
{
	ShiftCombination pattern;
	std::shared_ptr<Nurse> fullTimeNurse = std::make_shared<Nurse>(0, 36, true);
	std::shared_ptr<Nurse> partTimeNurse = std::make_shared<Nurse>(1, 20, true);
	setPattern(pattern, "DDDNNRN");
	EXPECT_EQ(softConstraints.getCostOfNightSeries(pattern, fullTimeNurse), 0);
	setPattern(pattern, "RRDNNNR");
	EXPECT_EQ(softConstraints.getCostOfNightSeries(pattern, partTimeNurse), 0);
	setPattern(pattern, "RRDNDDD");
	EXPECT_EQ(softConstraints.getCostOfNightSeries(pattern, fullTimeNurse), 0);
	setPattern(pattern, "RRDNDDD");
	EXPECT_EQ(softConstraints.getCostOfNightSeries(pattern, partTimeNurse), 1000);
}

TEST_F(SoftConstraintsTest, isShiftRangePerWeekCorrectlyChecked)
{
	ShiftCombination pattern;
	ShiftCombination twoWeeks(14);
	std::shared_ptr<Nurse> fullTimeNurse = std::make_shared<Nurse>(0, 36, true);
	std::shared_ptr<Nurse> partTimeNurse = std::make_shared<Nurse>(1, 20, true);
	setPattern(pattern, "DDDNNRN");
	EXPECT_EQ(softConstraints.getCostOfShiftsNumberPerWeek(pattern, fullTimeNurse), 10);
	setPattern(pattern, "DRDRRDD");
	EXPECT_EQ(softConstraints.getCostOfShiftsNumberPerWeek(pattern, partTimeNurse), 0);
	setPattern(pattern, "RRRDNNR");
	EXPECT_EQ(softConstraints.getCostOfShiftsNumberPerWeek(pattern, fullTimeNurse), 10);
	setPattern(twoWeeks, "RRRDNNRDDDDDDD");
	EXPECT_EQ(softConstraints.getCostOfShiftsNumberPerWeek(twoWeeks, fullTimeNurse), 30);
	setPattern(twoWeeks, "DRDRDRDNNNDDRD");
	EXPECT_EQ(softConstraints.getCostOfShiftsNumberPerWeek(twoWeeks, fullTimeNurse), 10);
	setPattern(pattern, "RRDNDDD");
	EXPECT_EQ(softConstraints.getCostOfShiftsNumberPerWeek(pattern, partTimeNurse), 0);
	setPattern(pattern, "DDDNDDD");
	EXPECT_EQ(softConstraints.getCostOfShiftsNumberPerWeek(pattern, partTimeNurse), 0);
}

TEST_F(SoftConstraintsTest, isShiftSeriesCorrectlyChecked)
{
	ShiftCombination pattern;
	ShiftCombination miniSchedule(14);
	std::shared_ptr<Nurse> fullTimeNurse = std::make_shared<Nurse>(0, 36, true);
	std::shared_ptr<Nurse> partTimeNurse = std::make_shared<Nurse>(1, 20, true);
	setPattern(pattern, "DDDRNNN");
	EXPECT_EQ(softConstraints.getCostOfShiftsSeries(pattern, partTimeNurse), 0);
	setPattern(pattern, "RDDDRNN");
	EXPECT_EQ(softConstraints.getCostOfShiftsSeries(pattern, fullTimeNurse), 10);
	setPattern(pattern, "DDDDDDD");
	EXPECT_EQ(softConstraints.getCostOfShiftsSeries(pattern, fullTimeNurse), 20);
	setPattern(miniSchedule, "RRDNDDDRDDDNRR");
	EXPECT_EQ(softConstraints.getCostOfShiftsSeries(miniSchedule, fullTimeNurse), 0);
	setPattern(miniSchedule, "DDDNDDDRDDDNRR");
	EXPECT_EQ(softConstraints.getCostOfShiftsSeries(miniSchedule, fullTimeNurse), 20);
	setPattern(miniSchedule, "DDDNDDDRDDDNRR");
	EXPECT_EQ(softConstraints.getCostOfShiftsSeries(miniSchedule, partTimeNurse), 0);
	setPattern(miniSchedule, "DRDNRDDRDDDNRR");
	EXPECT_EQ(softConstraints.getCostOfShiftsSeries(miniSchedule, fullTimeNurse), 40);

}

TEST_F(SoftConstraintsTest, isEarlyAfterDayShiftCorrectlyChecked)
{
	ShiftCombination pattern;
	setExtendedPattern(pattern, "EDEDLDL");
	EXPECT_EQ(softConstraints.getCostOfEarlyAfterDayShift(pattern), 5);
	setExtendedPattern(pattern, "EDEDRNNNENRDNEDLEDELD");
	EXPECT_EQ(softConstraints.getCostOfEarlyAfterDayShift(pattern), 10);
	setExtendedPattern(pattern, "REDEDEDERNDDELLELDEDN");
	EXPECT_EQ(softConstraints.getCostOfEarlyAfterDayShift(pattern), 25);
}

TEST_F(SoftConstraintsTest, isEarlyShiftSeriesCorrectlyChecked)
{
	ShiftCombination pattern;
	setExtendedPattern(pattern, "EDEDLDL");
	EXPECT_EQ(softConstraints.getCostOfEarlyShiftSeries(pattern), 10);
	setExtendedPattern(pattern, "NNRRRLD");
	EXPECT_EQ(softConstraints.getCostOfEarlyShiftSeries(pattern), 0);
	setExtendedPattern(pattern, "EDEDRNNNENRDNEDLEDELD");
	EXPECT_EQ(softConstraints.getCostOfEarlyShiftSeries(pattern), 50);
	setExtendedPattern(pattern, "EEREEEDEEEELLE");
	EXPECT_EQ(softConstraints.getCostOfEarlyShiftSeries(pattern), 10);
}